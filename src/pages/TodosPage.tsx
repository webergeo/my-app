import React, { useEffect, useState } from 'react';

import { ITodo } from '../interfaces';

import { TodoForm } from '../components/TodoForm'
import { TodoList } from '../components/TodoList'

export const TodosPage: React.FC = () => {
  const [todos, setTodos] = useState<ITodo[]>([])

  useEffect(() => {
    const saved = JSON.parse(localStorage.getItem('todos') || '[]') as ITodo[]
    setTodos(saved)
  }
    , []);

  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos))
  }, [todos]);

  const addHanlder = (title: string) => {
    const newTodo: ITodo = {
      title: title,
      id: Date.now(),
      completed: false
    }
    setTodos(prev => [newTodo, ...prev])
  }

  const toggleHandler = (id: number) => {
    setTodos(prev => prev.map(todo => {
      if (todo.id === id)
        return { ...todo, completed: !todo.completed, title: todo.title.endsWith('_UPDATE') ? todo.title : todo.title + '_UPDATE' }
      return todo;
    })
    );
  }
  const removeHandler = (id: number) => {

    const shouldRemove = window.confirm('Are you sicher?');
    if (shouldRemove)
      setTodos(prev => prev.filter(todo => todo.id !== id));
  };

  return <React.Fragment>
    <div className="container" >
      <TodoForm onAdd={addHanlder} />
      <TodoList todos={todos} onToggle={toggleHandler} onRemove={removeHandler} />
    </div>
  </React.Fragment>
}