import React, { useRef } from 'react';

interface TodoFormProps {
    onAdd(title: string): void;
}

export const TodoForm: React.FC<TodoFormProps> = (props) => {

    const ref = useRef<HTMLInputElement>(null)

    const onKeyPressHandler = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            props.onAdd(ref.current!.value)
            ref.current!.value = ''
        }
    }

    return (
        <div className="input-field mt2">
            <input
                ref={ref}
                type="text" id="title" placeholder="input taskname"
                onKeyPress={onKeyPressHandler} />
            <label htmlFor="title" className="active"> Taskname?</label>
        </div>
    )
}